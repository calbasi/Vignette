# ViĞnette

ViĞnette is a web utility that creates posters for Ğ1 currency.
It can also be used to create business cards.
Posters / cards will display :
- a title
- a description
- a photo (or Ğ symbol)
- the public keywith the checksum (or a short form on business cards)
- the QR-code of the public key

To install ViĞnette, simply clone the repo (or uncompress the release archive) $
If the displayed URL is too long to be written horizontally, just change the UR$


# ViĞnette (fr)

ViĞnette est un petit site qui permet aux usagers de créer des affichettes pour les Ğmarchés.
Il permet également de créer des planches de cartes de visite.
Ces affichettes indiquent :
* un titre
* une description
* une photo (ou un symbole Ğ)
* la clef publique avec son checksum
* le QR-code de la clef publique

## Installation

Pour installer ViĞnette, simplement décompresser l'archive sur son serveur.
Une fois installé, vous pouvez modifier le paramètre URL_ORIENTATION à la ligne 12 de index.html.
Il permet de choisir l'orientation de l'URL et de choisir de ne pas l'afficher.

## Release

L'exécution du script de build n'est pas nécessaire.
Il permet simplement de créer des fichiers un peu plus légers.

Dépendances nécessaires (sous Mint): 
` minify `

Exécution dans le dossier principal :
`$ bash ./build.sh`

# Credits

Original code by matograine at https://git.duniter.org/matograine/vignette

Forked by calbasi to add more languages: https://forum.duniter.org/t/vignette-creation-daffichettes-pour-gmarches/7994/5

