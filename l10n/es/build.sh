#!/bin/bash

# This script builds a release folder for vignette.
# Releases should be kept small to avoid unnecessary data transfer.
# work has to be done on materialize JS files.

temp_dir="temp"
build_dir="build"

rm -r $build_dir
mkdir $build_dir

cp index.html $build_dir
cp questions.html $build_dir
cp licence.html $build_dir

cp -r img/ $build_dir

mkdir $build_dir/theme
cp theme/materialize.min.css $build_dir/theme/
cp theme/materialize.min.js $build_dir/theme/

mkdir $build_dir/scr
minify scr/duniter_tools.js > $build_dir/scr/duniter_tools.js
minify scr/main.js > $build_dir/scr/main.js
minify scr/qrcode.js > $build_dir/scr/qrcode.js
